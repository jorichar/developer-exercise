class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)

        returnStr = "";
        marklarArray = str.split(/ /)
        marklarLength = marklarArray.length - 1

        for i in 0..marklarLength do
            local = marklarArray[i]
            
            # Find strings > 4 chars
            if local.length > 4

                localEnd = local.length - 1
                chr = local[0].chr
                prefix = ""
                suffix = ""
                le = local[localEnd]
                
                # See if we need to preserve punctuation   
                if /[^A_Z, a-z]/.match(le) then
                    suffix = le
                end
                
                # Preserve capitalization
                if chr=== chr.capitalize then
                    prefix += "M"
                else
                    prefix += "m"
                end

                returnStr += prefix + "arklar" + suffix
           
            # Any string < 4 in length stays as is
            else
                returnStr += local
            end
            
            # Add space back in except at end
            if i < marklarLength
                returnStr += " "
            end

            
        end #for loop

        return returnStr

      
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)

    fibArray = Array.new

    total = 0
    fibEnd = nth - 1

    # Base case
    if nth < 2 then 
        return 0
    end

    # Put fib sequence in array
    fibArray[0] = 1
    fibArray[1] = 1
    for i in 2..fibEnd do
        prevOne = i - 1
        prevTwo = i - 2
        fibArray[i] = fibArray[prevOne] + fibArray[prevTwo]

    end

    # Add up all evens in array
    for i in 0..fibEnd do
        if fibArray[i] % 2 == 0 then
            total += fibArray[i]
        end
    end

    return total
  
  end

end
